python manage.py makemigrations --noinput
python manage.py migrate --noinput
python manage.py collectstatic --noinput
chmod  +x  /usr/src/app/static
chmod +x /usr/src/app/media
gunicorn project.wsgi:application --bind 0.0.0.0:8000
